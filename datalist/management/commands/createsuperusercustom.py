from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

class Command(BaseCommand):

    help = 'Create a superuser if it does not exist'

    def handle(self, *args, **options):
        username = 'admin'
        password = 'password'
        email = 'admid@admin.com'

        # Check if the user already exists
        if not User.objects.filter(username=username).exists():
            # Create the superuser
            User.objects.create_superuser(username, email, password)
            self.stdout.write(self.style.SUCCESS('Superuser created successfully'))
        else:
            self.stdout.write(self.style.WARNING('Superuser already exists'))
