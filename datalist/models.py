from django.utils import timezone, formats

from auditlog.registry import auditlog
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models

class AdministrativeDocument(models.Model):
    technical = models.ForeignKey('Technical', related_name='docs', on_delete=models.CASCADE)
    document = models.FileField(upload_to='docs/')

class Technical(models.Model):
    CATEGORY = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
    )

    STATUS_CHOICES = (
        ('Встановіть статус', 'Встановіть статус'),
        ('Введено в експлуатацію', 'Введено в експлуатацію'),
        ('На складі', 'На складі'),
    )

    UNIT_MEASUREMENT_CHOICES = (
        ('шт.', 'шт.'),
        ('к-т.', 'к-т.'),
    )

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    product_name = models.CharField(max_length=100)
    factory_number = models.CharField(max_length=50, null=True, blank=True, default='---')
    inv_number = models.CharField(max_length=50, null=True, blank=True, default='---')
    quantity = models.IntegerField()
    unit_measurement = models.CharField(max_length=20, choices=UNIT_MEASUREMENT_CHOICES, default='шт.')
    price = models.DecimalField(max_digits=12, decimal_places=2)
    storage_location = models.CharField(max_length=100)
    responsible_person = models.CharField(max_length=50, null=True, default='---')
    fixed_person = models.CharField(max_length=50, null=True, default='---')
    item_passport_number = models.CharField(max_length=50, null=True, blank=True, default='---')
    date_of_start = models.DateField(null=True, blank=True, default=timezone.now)
    term_of_use = models.IntegerField()
    term_of_use_document = models.CharField(max_length=100, default='---')
    category = models.CharField(max_length=20, choices=CATEGORY, default='---')
    status = models.CharField(max_length=25, choices=STATUS_CHOICES, default='---')
    terms = models.CharField(max_length=200, blank=True, default='---')
    image = models.ImageField(upload_to='image/', null=True, blank=True)

    def formatted_date_of_start(self):
        if self.date_of_start:
            return self.date_of_start.strftime('%d.%m.%Y')
        return ""

    def formatted_price(self):
        if self.price:
            formatted_price = '{:,.2f}'.format(self.price).replace(',', ' ').replace('.', ',')
            return formatted_price
        return ""

auditlog.register(Technical)
auditlog.register(AdministrativeDocument)
