import qrcode
from django.http import HttpResponse
from datalist.models import Technical


def get_item_data(request):
    item_id = request.GET.get('id')
    item = Technical.objects.get(id=item_id)

    item_data = {
        'Найменування  об’єкта(ів)': item.product_name,
        'Заводський №': item.factory_number,
        'Інв. (номенклатур.) №': item.inv_number,
        'Закріплено за (особа)': item.fixed_person,
        '№ формуляра (паспорта)': item.item_passport_number,
        'Дата початку експлуатації': item.date_of_start,
        'Встановлений строк служби об’єкта(ів) в роках': item.term_of_use,
    }

    return item_data

def generate_qr_code(item_data_text):
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=5,
        border=4,
    )
    qr.add_data(item_data_text)
    qr.make(fit=True)
    qr_image = qr.make_image()

    return qr_image

def create_QR_from_item_id(request):
    item_data = get_item_data(request)
    item_data_text = '\n'.join([f'{key}: {value}' for key, value in item_data.items()])

    qr_image = generate_qr_code(item_data_text)

    # Create HTTP response with the QR code image
    response = HttpResponse(content_type='image/png')
    qr_image.save(response, 'PNG')

    return response