# Generated by Django 4.2.3 on 2024-05-21 10:26

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('datalist', '0025_alter_technical_status_alter_technical_terms'),
    ]

    operations = [
        migrations.AlterField(
            model_name='technical',
            name='date_of_start',
            field=models.DateField(blank=True, default=django.utils.timezone.now, null=True),
        ),
    ]
