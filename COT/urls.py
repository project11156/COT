"""
URL configuration for COT project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from datalist.generate_QR_code_service import create_QR_from_item_id
from datalist.views import user_login, user_logout, datalist, search, create_item, success_page, \
    change_item, export_excel, upload_file

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin', admin.site.urls),
    path('login', user_login, name='login'),
    path('logout', user_logout, name='logout'),
    path('', datalist),
    path('itemQRCode', create_QR_from_item_id),
    path('search', search, name='search'),
    path('create_item/', create_item, name='create_item'),
    path('change_item/<int:item_id>/', change_item, name='change_item'),
    path('success', success_page, name='success_page'),
    path('export/excel/', export_excel, name='export_excel'),
    path('upload/', upload_file, name='upload_page'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
